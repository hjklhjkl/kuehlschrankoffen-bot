import datetime
import logging

import telegram
import telegram.ext


start_time = str(datetime.datetime.now().isoformat())[:-7]  # cut off subseconds
open_str = 'offen'
closed_str = 'geschlossen'


class Bot:
    chat_id = 0
    bot_token = 'TOKEN'

    def __init__(self, refrigerator, reset_function):  # timeout in seconds
        self.refrigerator = refrigerator
        self.updater = telegram.ext.Updater(token=self.bot_token, use_context=True)
        self.updater.dispatcher.add_handler(telegram.ext.CommandHandler('status', self.status))
        self.main_reset = reset_function
        self.updater.dispatcher.add_handler(telegram.ext.CommandHandler('reset', self.reset))
        self.updater.start_polling()

    def send(self, message):
        logging.info('Telegram message sent: "{0}"'.format(message))
        self.updater.bot.send_message(text=message, chat_id=self.chat_id)

    def reset(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        self.main_reset()
        context.bot.send_message(chat_id=update.effective_chat.id, text="Reset done.")

    def status(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        frige_status = closed_str if self.refrigerator.is_frige_closed() else open_str
        freezer_status = closed_str if self.refrigerator.is_freezer_closed() else open_str
        status_msg = 'Online seit:\n{stime}\n{frigename}: {frigestatus}\n{freezername}: {freezerstatus}\n' \
                     '{frigename}zyklen: {frigecycle}\n{freezername}zyklen: {freezercycle}\n' \
                     '{frigename} gesamt offene Zeit: {frigetime}s\n{freezername} gesamt offene Zeit: {freezertime}s\n'\
                     'Letzte MQTT Nachricht: {lastmqtt}'\
            .format(stime=start_time, frigename=self.refrigerator.frige_name(), frigestatus=frige_status,
                    freezername=self.refrigerator.freezer_name(), freezerstatus=freezer_status,
                    frigecycle=self.refrigerator.frige_cycles,
                    freezercycle=self.refrigerator.freezer_cycles,
                    frigetime=int(self.refrigerator.total_frige_open_time),
                    freezertime=int(self.refrigerator.total_freezer_open_time),
                    lastmqtt=datetime.datetime.fromtimestamp(self.refrigerator.last_mqtt_time).isoformat()[:-7])
        context.bot.send_message(chat_id=update.effective_chat.id, text=status_msg)

    def stop(self):
        self.updater.stop()
