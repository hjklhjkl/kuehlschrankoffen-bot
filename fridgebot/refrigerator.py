import time

frige_name = 'Kühlfach'
freezer_name = 'Gefrierfach'


class Refrigerator:
    def __init__(self):
        self.frige_closed = True
        self.freezer_closed = True
        self.frige_cycles = 0
        self.freezer_cycles = 0
        self.total_frige_open_time = 0
        self.total_freezer_open_time = 0
        self.last_frige_open = 0
        self.last_freezer_open = 0
        self.last_mqtt_time = 0

    def set_frige_open(self):
        self.last_frige_open = time.time()
        self.frige_closed = False

    def set_frige_closed(self):
        if self.last_frige_open != 0:
            self.total_frige_open_time += time.time() - self.last_frige_open
        self.frige_cycles += 1
        self.frige_closed = True

    def is_frige_closed(self):
        return self.frige_closed

    def set_freezer_open(self):
        self.last_freezer_open = time.time()
        self.freezer_closed = False

    def set_freezer_closed(self):
        if self.last_freezer_open != 0:
            self.total_freezer_open_time += time.time() - self.last_freezer_open
        self.freezer_cycles += 1
        self.freezer_closed = True

    def is_freezer_closed(self):
        return self.freezer_closed

    def frige_name(self):
        return frige_name

    def freezer_name(self):
        return freezer_name

    def register_mqtt_msg(self):
        self.last_mqtt_time = time.time()
