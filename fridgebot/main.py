import logging
import os
import time

import paho.mqtt.client
from bot import Bot
from refrigerator import Refrigerator
from watchdog import Watchdog

logging_dir = ''.join([os.path.expanduser('~'), '/.cache/kuehlschrank-bot'])
if not os.path.isdir(logging_dir):
    os.mkdir(logging_dir)
logging.basicConfig(filename=''.join([logging_dir, '/kb.log']), encoding='utf-8', level=logging.INFO,
                    format='%(asctime)s %(message)s')


def watchdog_reset():
    logging.info('Stopped watchdogs')
    freezer_watchdog.stop()
    fridge_watchdog.stop()


refrigerator = Refrigerator()
tbot = Bot(refrigerator, watchdog_reset)
freezer_watchdog = Watchdog(tbot, refrigerator.freezer_name())
fridge_watchdog = Watchdog(tbot, refrigerator.frige_name())


def on_message(client, userdata, message):
    refrigerator.register_mqtt_msg()
    topic = message.topic
    msg = str(message.payload.decode('utf-8'))
    if topic == 'FREEZER_DOOR':
        if msg == '1' and not freezer_watchdog.is_running():
            refrigerator.set_freezer_open()
            freezer_watchdog.start()
        elif msg == '0' and freezer_watchdog.is_running():
            refrigerator.set_freezer_closed()
            freezer_watchdog.stop()
    elif topic == 'FRIDGE_DOOR':
        if msg == '1' and not fridge_watchdog.is_running():
            refrigerator.set_frige_open()
            fridge_watchdog.start()
        elif msg == '0' and fridge_watchdog.is_running():
            refrigerator.set_frige_closed()
            fridge_watchdog.stop()
    logging.info('received message from topic %s: %s' % (topic, msg))


mqttBroker = '192.168.178.39'

client = paho.mqtt.client.Client('kuelschrankoffen_bot')
client.connect(mqttBroker)
logging.info('Connected to MQTT broker at {0}'.format(mqttBroker))
client.loop_start()

client.subscribe([('FRIDGE_DOOR', 0), ('FREEZER_DOOR', 0)])
client.on_message = on_message

logging.info('Fridgebot is running...')

uninterrupted = True
while uninterrupted:
    try:
        time.sleep(1)
    except KeyboardInterrupt:
        logging.info('Interrupt recieved')
        uninterrupted = False

client.loop_stop()
logging.info('Stopped MQTT client loop')
tbot.stop()
logging.info('Stopped Telegram bot')
logging.warning('Exiting after interrupt')
