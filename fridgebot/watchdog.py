import logging
from threading import Timer


class Watchdog:
    timeout = 60    # in seconds
    cycles = 0
    running = False

    def __init__(self, bot, topic_name=None):
        self.bot = bot
        self.topic_name = topic_name if topic_name is not None else 'Nothing'
        self.timer = Timer(self.timeout, self.handler)

    def start(self):
        logging.info('{0} timer restarted'.format(self.topic_name))
        self.running = True
        self.timer.start()

    def stop(self):
        logging.info('{0} timer stopped'.format(self.topic_name))
        self.timer.cancel()
        self.timer = Timer(self.timeout, self.handler)
        self.running = False
        if self.cycles > 0:
            message = '✅ Tür des {0}s ist nun wieder zu'.format(self.topic_name)
            self.cycles = 0
            self.bot.send(message)

    def handler(self):
        self.cycles = self.cycles + 1
        message = '🚨 Tür des {0}s ist seit {1} Minuten offen'.format(self.topic_name, str(self.cycles))
        self.bot.send(message)
        self.timer = Timer(self.timeout, self.handler)
        self.timer.start()

    def is_running(self):
        return self.running
