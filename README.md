# Python app that handles MQTT refrigerator info with a Telegram bot

## Build with venv

### Setting up

* Initializing venv environment `python3 -m venv ./venv`
* Activating venv environment `source venv/bin/activate`
* Installing dependencies `pip install -r requirements.txt`

### Building executable with pyinstaller

* Installing pyinstaller `pip install pyinstaller`
* Building executable `./venv/bin/pyinstaller --onefile --name kuehlschrankoffen-bot fridgebot/main.py`
